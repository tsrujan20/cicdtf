terraform {
    backend "s3" {
        bucket = "mys31990"
        key = "state"
        region = "us-east-2"
        dynamodb_table = "mybackend"
    }
}
