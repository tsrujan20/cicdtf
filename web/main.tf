#ec2

resource "aws_instance" "server" {
  ami                    = "ami-05fb0b8c1424f266b"      #change ami id for different region
  instance_type          = "t2.micro"
  key_name               = "linuxvm"              #change key name as per your setup
  subnet_id              =  var.sn
  security_groups        =  [var.sg]

  tags = {
    Name = "myserver"
  }


}
